package com.tvb.ppm;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import com.tvb.ppm.config.SystemConfiguration;
import com.tvb.ppm.helper.DataLoader;
import com.tvb.ppm.helper.ObjectLoader;
import com.tvb.ppm.report.PlainTextFileLogger;

public class PortalPageMonitor {

	public static void main(String[] args) {
		
		StopWatch stopWatch = new StopWatch();
		
		// Get the url list from file
		DataLoader loader = (DataLoader) ObjectLoader.createObject("dataLoader");
		List<String> urls = loader.getData();
		
		// Try to load the page and check the title
		WebDriver driver = null;
		if (args.length != 0) {
			if ("safari".equalsIgnoreCase(args[0])) {
				driver = new SafariDriver();
			} else if ("chrome".equalsIgnoreCase(args[0])) {
				driver = new ChromeDriver();
			} else if ("firefox".equalsIgnoreCase(args[0])) {
				driver = new FirefoxDriver();
			} else if ("ie".equalsIgnoreCase(args[0])) {
				//Method and Description - static DesiredCapabilities internetExplorer()`
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				//Method and Description - void setCapability(java.lang.String capabilityName, boolean value)
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				//Among the facilities provided by the System class are standard input, standard output, and error output streams; access to externally defined "properties"; a means of loading files and libraries; and a utility method for quickly copying a portion of an array.
				System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
				//InternetExplorerDriver(Capabilities capabilities)
				driver = new InternetExplorerDriver(capabilities);
			}
		} else {
			driver = new SafariDriver();
		}
		
		if (urls != null) {
			for (String url: urls) {
				stopWatch.start();
				driver.get(url);
				stopWatch.stop();
				PlainTextFileLogger.writeLog("Page title is: " + driver.getTitle());
				PlainTextFileLogger.writeLog("Page Response time is: " + stopWatch.toString());
				stopWatch.reset();
				
				try {
					Thread.sleep(SystemConfiguration.getConfigLong("waitTime", 120000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				
				String bgAdWallPaperPath = null;
				try {
					WebElement bgAdWallPaperDiv = driver.findElement(By.xpath("//div[@id='big-body-bg']"));
					String background = bgAdWallPaperDiv.getCssValue("background");
					PlainTextFileLogger.writeLog("background = " + background);
					if (StringUtils.isNotBlank(background)) {
						if ("firefox".equalsIgnoreCase(args[0])) {
							//PlainTextFileLogger.writeLog("size = " + background.length() + ", start = " + (background.indexOf("url(")+5) + ", end = " + (background.indexOf(") no-repeat")-1));
							bgAdWallPaperPath = background.substring(background.indexOf("url(")+5, background.indexOf(") no-repeat")-1);
						} else {
							bgAdWallPaperPath = background.substring(background.indexOf("url(")+4, background.indexOf("); background-attachment"));
						}
						PlainTextFileLogger.writeLog("bgAdWallPaperPath = " + bgAdWallPaperPath);
					}
				} catch (NoSuchElementException e) {
					System.out.println(e.getMessage());
				}
				
				String adImgPath = null;
				try {
					WebElement leftAdDiv = driver.findElement(By.xpath("//div[@id='ad-lrec']"));
					if (leftAdDiv != null) {
						WebElement adImage = leftAdDiv.findElement(By.tagName("img"));
						adImgPath = adImage.getAttribute("src");
						System.out.println(adImgPath);
					}
				} catch (NoSuchElementException e) {
					System.out.println(e.getMessage());
				}
				
				if (StringUtils.isNotBlank(bgAdWallPaperPath)) {
					stopWatch.start();
					driver.get(bgAdWallPaperPath);
					stopWatch.stop();
					PlainTextFileLogger.writeLog("Background Ad Response time is: " + stopWatch.toString());
					stopWatch.reset();
				}
				
				if (StringUtils.isNotBlank(adImgPath)) {
					stopWatch.start();
					driver.get(adImgPath);
					stopWatch.stop();
					PlainTextFileLogger.writeLog("Large Rect Ad Response time is: " + stopWatch.toString());
					stopWatch.reset();
				}
				
			}
		}
		driver.quit();
	}
}