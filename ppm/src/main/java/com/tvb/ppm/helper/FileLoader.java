package com.tvb.ppm.helper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FileLoader implements DataLoader {
	
	private String fileName;
	
	public void setResourceName(String resourceName) {
		this.fileName = resourceName;
	}

	public List<String> getData() {
		List<String> urls = null;
		File file = new File(fileName);
		try {
			urls = FileUtils.readLines(file, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return urls;
	}
}