package com.tvb.ppm.helper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;

import com.tvb.ppm.config.SystemConfiguration;
import com.tvb.ppm.model.PageList;

public class URLLoader implements DataLoader{
	
	private String urlPath;
	
	@Override
	public void setResourceName(String resourceName) {
		this.urlPath = resourceName;
	}
	
	public List<String> getData() {
		
		List<String> urlList = null;
		
		try {
			// create JAXB context and initializing Marshaller
			JAXBContext jaxbContext = JAXBContext.newInstance(PageList.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			// specify the location and name of xml file to be read
			String threadNo = SystemConfiguration.getConfig("threadNo", "1");
			urlPath = urlPath + threadNo;
			File xmlfile = new File("temp.xml");
			try {
				URL url = new URL(urlPath);
				FileUtils.copyURLToFile(url, xmlfile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			// this will create Java object - country from the XML file
			PageList pageList = (PageList) jaxbUnmarshaller.unmarshal(xmlfile);
			
			urlList = pageList.getUrl();

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return urlList;
	}
}
