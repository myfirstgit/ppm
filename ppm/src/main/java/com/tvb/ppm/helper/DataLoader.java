package com.tvb.ppm.helper;

import java.util.List;

public interface DataLoader {
	public List<String> getData();
	public void setResourceName(String resourceName);
}
