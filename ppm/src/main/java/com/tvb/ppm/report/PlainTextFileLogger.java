package com.tvb.ppm.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tvb.ppm.config.SystemConfiguration;

public class PlainTextFileLogger {

	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final String DEFAULT_CHARSET = "utf-8";
	public static final String OUTPUT_FILE_CONFIG = "outputFile";
	public static final String OUTPUT_FILE_DEFAULT_NAME = "report.log";

	public static final String OUTPUT_FILE_NAME = SystemConfiguration.getConfig(OUTPUT_FILE_CONFIG, OUTPUT_FILE_DEFAULT_NAME);
	public static FileOutputStream writer;

	static {
		try {
			writer = new FileOutputStream(OUTPUT_FILE_NAME, true);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void writeLog(String content) {
		try {
			writer.write(SDF.format(new Date()).getBytes(DEFAULT_CHARSET));
			writer.write(' ');
			writer.write(content.getBytes(DEFAULT_CHARSET));
			writer.write('\n');
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
