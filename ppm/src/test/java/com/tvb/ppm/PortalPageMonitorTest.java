package com.tvb.ppm;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class PortalPageMonitorTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PortalPageMonitorTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PortalPageMonitorTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	//assertTrue( TVBComExample.getResponseTime() == 0 );
    }
}
